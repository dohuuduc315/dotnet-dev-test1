﻿namespace SortArrayDESC;

public class Program
{
    static void Main(string[] args)
    {
        var sorted = SortArrayDesc(new int[] { 1 });
        Console.WriteLine(sorted);
    }

    public static int[] SortArrayDesc(int[] x)
    {
        //kết quả hàm sau khi thực thi là mảng số nguyên mới đã sắp xếp theo thứ tự giảm dần
        Array.Sort(x, (a, b) => b.CompareTo(a));
        foreach (int i in x)
        {
            Console.Write(i + " ");
        }
        return x;
    }

}