## bài tập
Cho hai chuỗi, hãy viết một phương thức trả về một chuỗi mới gồm hai chuỗi. 
Chữ cái đầu tiên của chuỗi mới là chữ cái đầu tiên của chuỗi thứ nhất,
chữ cái thứ hai của chuỗi mới là chữ cái đầu tiên của chuỗi thứ hai, v.v.
ví dụ:
MixTwoStrings("aaa", "BBB") → "aBaBaB"
MixTwoStrings("good one", "111") → "g1o1o1d one"


## lưu ý: khi nộp bài source code phải luôn luôn build thành công.

## kiểm tra kết quả của mình bằng cách chạy unit test
	trên thanh menu của Visual studio -> Test -> Test Explorer -> chạy test để kiểm tra kết quả

