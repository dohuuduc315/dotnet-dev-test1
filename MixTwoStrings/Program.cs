﻿using System.Text;

var result = PracticeStrings.MixTwoStrings("", "");
Console.WriteLine(result);
Console.WriteLine("hello world");

public class PracticeStrings
{

    public static string MixTwoStrings(string value1, string value2)
    {
        // xem file README.md
        var stringBuilder = new StringBuilder();
        for (int i = 0; i < Math.Max(value1.Length, value2.Length); i++)
        {
            if (i < value1.Length)
                stringBuilder.Append(value1[i]);

            if (i < value2.Length)
                stringBuilder.Append(value2[i]);
        }

        string result = stringBuilder.ToString();
        return stringBuilder.ToString();
    }
}

