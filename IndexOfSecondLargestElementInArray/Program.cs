﻿namespace IndexOfSecondLargestElementInArray;

public class Program
{
    static void Main(string[] args)
    {
        var myArr = new[] { 1, 2, 3 };
        var result = IndexOfSecondLargestElementInArray(myArr);
        Console.WriteLine(result);
    }

    public static int IndexOfSecondLargestElementInArray(int[] x)
    {
        // kết quả hàm sau khi thực thi là vị trí(index) của số lớn thứ 2 trong mảng
        int i = 0, j = 0, largestNumber = 0, SecondNumber = 0, result = 0;
        if (x.Length == 0 || x.Length == 1)
        {
            return -1;
        }
        else
        {
            for (i = 0; i < x.Length; i++)
            {
                if (largestNumber < x[i])
                {
                    largestNumber = x[i];
                    j = i;
                }
            }
            for (i = 0; i < x.Length; i++)
            {
                if (i == j)
                {
                    i++;
                    i--;
                }
                else
                {
                    if (SecondNumber < x[i])
                    {
                        SecondNumber = x[i];
                        result = i;
                    }
                }
            }
            return result;
        }
    }
}